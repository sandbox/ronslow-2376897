
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration
 * JSON Data mapping
 


INTRODUCTION
------------

Current Maintainer: PrivacyBuilder (ronslow@privacybuilder.eu)

PrivacyBuilder is a subscription service at https://privacybuilder.eu which
manages user consents to the use of personal data by a web application.

This plugin integrates the PrivacyBuilder service into Drupal.
The plugin adds consent widgets, which have previously been defined in
PrivacyBuilder, to the registration page and user profile page in Drupal.

Thereafter, the plugin uses the PrivacyBuilder service to  manage the
deletion of user data in Drupal following the withdrawal of consent by the user.



INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
 * In PrivacyBuilder configuration page, enter your PrivacyBuilder
subscription username and password and JSON data mapping (see below)
 * In People -> Permissions -> Cancel Own Account, tick Authenticated User

JSON DATA MAPPING
-----------------
If your website adds user data to the $account['data'] array,
then this data may need to be deleted when a user withdraws a consent.

When a user withdraws a consent provided by this plugin,
the plugin will query PrivacyBuilder for the "infofields" to be deleted.
The website developer needs to provide a map from the infofields
defined in PrivacyBuilder to the specific
variable names defined by the website developer in Drupal.

Use valid JSON to do that in the configuration screen.
By valid, that means "double" quotes"

TROUBLESHOOTING
---------------
 * If the consent widgets do not display, check the following:


   - PrivacyBuilder credentials are correct


   - Valid JSON (or none) in the JSON Data mapping field
